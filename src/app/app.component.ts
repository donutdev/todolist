import { Component, OnInit } from '@angular/core';
import { ResMovie } from 'src/assets/interface/movie';
import { LocalService } from 'src/assets/local.service';
import Swal from 'sweetalert2';
import * as uuid from 'uuid';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public closeResult = '';
  public movie: ResMovie = {
    name: '',
    status: false,
    id: '',
  };

  public listMovie: ResMovie[] = [];
  public listNameMovies: ResMovie[] = [];
  public listMovies: ResMovie[] = [];
  public newName = '';
  public idData = '';
  constructor(private localService: LocalService) {}

  ngOnInit(): void {
    this.getDataMovie();
  }
  // สร้างฟังก์ชันเพื่อเพิ่มข้อมูล
  async addMovie() {
    // console.log('addmovie', this.movie);
    // console.log('addmovie', this.movie);

    if (this.movie.name) {
      if (this.listNameMovies.length > 0) {
        const myId = uuid.v4();
        this.movie.id = myId;
      }
      // this.movie.id = this.listNameMovies.length + 1;
      const fixData = this.localService.getMovie();
      if (fixData) {
        this.listMovies = JSON.parse(fixData);
        this.listMovies.push(this.movie);
        this.localService.setMovie(this.listMovies);
      }
      if (!fixData) {
        this.listMovies.push(this.movie);
        this.localService.setMovie(this.listMovies);
      }
      this.getDataMovie();
      await Swal.fire({
        icon: 'success',
        title: 'เพิ่มข้อมูลสำเร็จ',
        showConfirmButton: false,
        timer: 1500,
      });
      this.movie.name = '';
    } else {
      Swal.fire({
        icon: 'error',
        title: 'กรุณากรอกข้อมูล',
        text: 'กรุณากรอกข้อมูลให้ครบถ้วนก่อนกดบันทึก',
      });
    }
  }

  // สร้างฟังก์ชันเพื่อลบข้อมูล
  clearForm() {
    for (const iterator of this.listNameMovies) {
      iterator.status = false;
      this.localService.setMovie(this.listNameMovies);
    }
  }

  // สร้างฟังก์ชันในการเลือกข้อมูล
  convertStringBoolean(i: number) {
    // console.log('convertStringBoolean', this.listNameMovies[i]);
    const isCheck: any = this.listNameMovies[i].status;
    this.listNameMovies[i].status = status === 'false' ? false : true;
  }

  // สร้างฟังก์ชันในการลบข้อมูล
  deleteMovie(id: string) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: 'ต้องการลบหรือไม่?',
        text: 'กรุณาตรวจสอบก่อนทำรายการ!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'ใช่ ลบเลย!',
        cancelButtonText: 'ไม่ใช่',
        reverseButtons: true,
      })
      .then(async (result) => {
        if (result.isConfirmed) {
          const dataDelete = this.listNameMovies.find((item) => item.id === id);
          if (dataDelete) {
            const index = this.listNameMovies.indexOf(dataDelete);
            this.listNameMovies.splice(index, 1);
            const sortData = this.listNameMovies.sort((a: any, b: any) => {
              return a.index > b.index ? 1 : -1;
            });
            this.localService.setMovie(sortData);
            await this.getDataMovie();
          }
          // console.log('deleteMovie', dataDelete);
          swalWithBootstrapButtons.fire(
            'ลบสำเร็จ!',
            'ทำการลบข้อมูลสำเร็จ',
            'success'
          );
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            'ลบไม่สำเร็จ',
            'ทำการลบข้อมูลไม่สำเร็จ',
            'error'
          );
        }
      });
  }

  //เรียกข้อมูลจาก localstorage
  getDataMovie() {
    // console.log('getdata', this.listMovies);
    const movie = this.localService.getMovie();
    if (movie) {
      this.listNameMovies = JSON.parse(movie).sort((a: any, b: any) => {
        return a.index > b.index ? 1 : -1;
      });
      // console.log('movie', this.listNameMovies);
    }
  }

  // สร้างฟังก์ชันเพื่อแก้ไขข้อมูล
  updateMovie(id: string) {
    // console.log('updateMovie', id);
    this.idData = id;

    const dataUpdate = this.listNameMovies.find((item) => item.id === id);
    // console.log('index', dataUpdate);
    if (dataUpdate) {
      this.newName = dataUpdate.name;
    }
  }

  upDataSuccess(id: string) {
    // console.log('idData', id);
    // console.log('idData', this.idData);

    for (const dataAll of this.listNameMovies) {
      if (dataAll.id === this.idData) {
        dataAll.name = this.newName;
        Swal.fire({
          icon: 'success',
          title: 'แก้ไขข้อมูลสำเร็จ',
          showConfirmButton: false,
          timer: 1500,
        });
        break;
      }
    }
    this.localService.setMovie(this.listNameMovies);
    // console.log('upDataSuccess', this.listNameMovies);
  }

  //ยืนยัน
  submitForm() {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: 'ต้องการบันทึกหรือไม่?',
        text: 'กรุณาตรวจสอบความถูกต้องก่อนทำการบันทึก!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'ใช่ ลบเลย!',
        cancelButtonText: 'ไม่ใช่',
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          // console.log('submitForm');
          const sortData = this.listNameMovies.sort((a: any, b: any) => {
            return a.index > b.index ? 1 : -1;
          });
          this.localService.setMovie(sortData);
          this.getDataMovie();

          swalWithBootstrapButtons.fire(
            'บันทึกข้อมูลสำเร็จ!',
            'ทำการบันทึกข้อมูลสำเร็จ!',
            'success'
          );
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            'บันทึกไม่สำเร็จ',
            'โปรดทำการบันทึกใหม่อีกครั้ง',
            'error'
          );
        }
      });
  }
}
