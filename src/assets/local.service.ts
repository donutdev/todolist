import { Injectable } from '@angular/core';
import { ResMovie } from './interface/movie';

@Injectable({
  providedIn: 'root',
})
export class LocalService {
  constructor() {}
  setMovie(movie: ResMovie []) {
    localStorage.setItem('movie', JSON.stringify(movie));
  }

  removeMovie() {
    localStorage.removeItem('movie');
  }
  // ─────────────────────────────────────────────────────────────────

  getMovie() {
    return localStorage.getItem('movie');
  }
}
