export interface ResMovie {
  id: string;
  name: string;
  status: boolean;
}
